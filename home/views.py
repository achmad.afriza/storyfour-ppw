from django.shortcuts import render

# Create your views here.
def index(request):
    context = {
        'nav': [
            ['#profile', 'About Me'],
        ],
    }

    return render(request, 'home.html', context)